
///
/// Rappresenta un rettangolo, utile per algoritmi
/// 
#[derive(Copy, Clone)]
pub struct Rect {
    pub beg_x: i32, pub beg_y: i32, 
    pub end_x: i32, pub end_y: i32
}

impl Rect {

    /// Crea un nuovo rettangolo
    pub fn new(x0: i32, y0: i32, w: i32, h: i32) -> Self {
        Self {
            beg_x: x0,
            beg_y: y0,
            end_x: x0 + w,
            end_y: y0 + h
        }
    }

    /// Controlla se due rettangoli si intersecano, anche aggiungendo un bordo
    pub fn intersect(&self, other: &Self, border: i32) -> bool {
        (self.beg_x - border) < (other.end_x + border) &&
        (self.end_x + border) > (other.beg_x - border) &&
        (self.beg_y - border) < (other.end_y + border) &&
        (self.end_y + border) > (other.beg_y - border)
    }

    /// Calcola il centro del rettangolo
    pub fn center(&self) -> (i32, i32) {
        ((self.beg_x + self.end_x) / 2, (self.beg_y + self.end_y) / 2)
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn rect_collision() {

        let a = Rect::new(0, 0, 10, 10);
        let b = Rect::new(5, 5, 10, 10);
        let c = Rect::new(15, 15, 5, 5);

        assert_eq!(a.intersect(&b), true);
        assert_eq!(b.intersect(&a), true);
        assert_eq!(a.intersect(&c), false);
        assert_eq!(c.intersect(&a), false);
    }
}