use crate::components::*;
use crate::command::*;
use crate::map::*;
use specs::prelude::*;

///
/// Sistema di visione
/// 
pub struct SightSystem;
impl<'a> System<'a> for SightSystem {

    ///
    /// Map:        per modificare la struttura cambiando i field di visibilità
    /// Entities:   per ottenere l'entità che corrisponde al giocatore
    /// Positions:  per calcolare la nuova visibilità
    /// Player:     per ottenere le entità giocatore
    /// Sight:      per le caratteristiche visive delle entità
    /// 
    type SystemData = (WriteExpect<'a, Map>, Entities<'a>, ReadStorage<'a, Position>,
        ReadStorage<'a, Player>, WriteStorage<'a, Sight>);

    /// Aggiorna coordinate visibili
    fn run(&mut self, (mut map, entities, positions, player, mut sights): Self::SystemData) {
        for (entity, position, sight) in (&entities, &positions, &mut sights).join() {
            
            // Ricalcola solo necessario
            if sight.dirty {
                sight.dirty = false;
                
                // Resetta celle visibili e ricalcola
                sight.visibles.clear();
                let point = rltk::Point::new(position.x, position.y);
                sight.visibles = rltk::field_of_view(point, sight.range, &*map);

                // Elimina coordinate al di fuori della mappa
                sight.visibles.retain(|p| map.inside(p.x, p.y));

                // Nel caso si tratti del giocatore dobbiamo aggiornare anche la mappa
                // TODO: Probabilmente sarebbe meglio scollegare questo sistema dalla mappa
                // ma per ora facciamo così...
                if let Some(_) = player.get(entity) {
                    map.update_visibility_fields(sight);
                }

            }
        }
    }
}

///
/// Sistema di AI per i mostri
/// TODO: probabilmente troppo grande e complesso, sarebbe bello dividerlo in un generatore
/// di azioni e poi lasciare che siano gli altri sistemi a eseguirle...
/// 
pub struct MonsterSystem;
impl<'a> System<'a> for MonsterSystem {

    ///
    /// Point:          posizione del giocatore, per pathfinding
    /// Monster:        per ottenere le entità mostro
    /// Position:       per modificare la posizione dei mostri
    /// Sight:          per il campo visivo dei mostri
    /// Name:           per identificare i mostri
    /// MoveCommand:    per inserire comandi di movimento
    /// AttackCommand:  per inserire comandi di combattimento
    /// 
    type SystemData = (ReadExpect<'a, Entity>, Entities<'a>, ReadStorage<'a, Monster>, ReadStorage<'a, Position>,
        WriteStorage<'a, Sight>, ReadStorage<'a, Name>, WriteStorage<'a, MoveCommand>, WriteStorage<'a, MeleeAttackCommand>);

    /// Anima i mostri
    fn run(&mut self, (player, entities, monsters, positions, mut sights, names, mut moves, mut attacks): Self::SystemData) {
        for (entity, _, position, sight, name) in (&entities, &monsters, &positions, &mut sights, &names).join() {

            let player_position = positions.get(*player).unwrap();
            let player_position = rltk::Point::new(player_position.x, player_position.y);
            
            // Se la posizione del giocatore è visibile allora reagisce alla sua presenza
            if sight.visibles.contains(&player_position) {
                //println!("il {} ha visto il giocatore e lo sta inseguendo...", name.0);

                let p0 = Map::xy_to_idx(position.x, position.y);
                let p1 = Map::xy_to_idx(player_position.x, player_position.y); 
                
                // Se abbastanza vicini attacca, altrimenti prova ad avvicinarti
                let distance = rltk::DistanceAlg::Pythagoras.distance2d(rltk::Point::new(position.x, position.y), player_position);
                if distance < 1.5 {
                    // Crea comando di combattimento
                    attacks.insert(entity, MeleeAttackCommand { target: *player })
                        .expect("Impossibile inserire comando di attacco");
                
                } else {
                    // Crea comando di movimento
                    moves.insert(entity, MoveCommand { new_idx: p1 })
                        .expect("Impossibile inserire comando di movimento");

                    // Il mostro deve ricalcolare la sua visibilità
                    sight.dirty = true;
                }
            }
        }
    }

}