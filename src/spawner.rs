use crate::components::*;
use specs::prelude::*;
use rand::prelude::*;

///
/// Facilita la creazione delle entità
/// 

/// Crea il giocatore in una posizione desiderata
pub fn player(x: i32, y: i32, world: &mut specs::World) -> specs::Entity {
    world.create_entity()
        .with(Position { x, y })
        .with(Renderable {
            glyph: rltk::to_cp437('@'),
            fg: rltk::RGB::named(rltk::YELLOW),
            bg: rltk::RGB::named(rltk::BLACK),
        })
        .with(Sight {
            visibles: vec![],
            range: 8,
            dirty: true
        })
        .with(Stats {
            max_hp: 100,
            hp: 100,
            atk: 6,
            def: 3
        })
        .with(Name("ziko"))
        .with(Player)
        .build()
}

/// Crea un goblin in una posizione desiderata
pub fn goblin(x: i32, y: i32, world: &mut specs::World) -> specs::Entity {
    world.create_entity()
        .with(Position { x, y })
        .with(Renderable {
            glyph: rltk::to_cp437('g'),
            fg: rltk::RGB::named(rltk::GREEN),
            bg: rltk::RGB::named(rltk::BLACK),
        })
        .with(Sight {
            visibles: vec![],
            range: 10,
            dirty: true
        })
        .with(Stats {
            max_hp: 5,
            hp: 5,
            atk: 2,
            def: 1
        })
        .with(Monster)
        .with(Name("goblin"))
        .with(Blocker)
        .build()
}

/// Crea un troll in una posizione desiderata
pub fn troll(x: i32, y: i32, world: &mut specs::World) -> specs::Entity {
    world.create_entity()
        .with(Position { x, y })
        .with(Renderable {
            glyph: rltk::to_cp437('T'),
            fg: rltk::RGB::named(rltk::GREEN_YELLOW),
            bg: rltk::RGB::named(rltk::BLACK),
        })
        .with(Sight {
            visibles: vec![],
            range: 6,
            dirty: true
        })
        .with(Stats {
            max_hp: 15,
            hp: 15,
            atk: 5,
            def: 3
        })
        .with(Monster)
        .with(Name("troll"))
        .with(Blocker)
        .build()
}

/// Crea un nemico completamente casuale a scelta fra i preset presenti
pub fn enemy(x: i32, y: i32, world: &mut specs::World) -> specs::Entity {
    let mut rng = rand::thread_rng();
    match rng.gen_range(0, 2) {
        0 => troll(x, y, world),
        _ => goblin(x, y, world)
    }
}