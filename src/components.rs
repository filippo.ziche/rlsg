use specs::prelude::*;

///
/// Rappresenta una generica posizione
/// 
#[derive(Component)]
#[storage(DenseVecStorage)]
pub struct Position { 
    pub x: i32,
    pub y: i32,
}

///
/// Tagga un'entità come giocatore
/// dovrebbe essercene solo una
/// 
#[derive(Component, Default)]
#[storage(NullStorage)]
pub struct Player;

///
/// Tagga un entità come mostro
/// 
#[derive(Component, Default)]
#[storage(NullStorage)]
pub struct Monster;

///
/// Indica un nome da usare per l'entità
/// 
#[derive(Component)]
#[storage(DenseVecStorage)]
pub struct Name(pub &'static str);

///
/// Rappresenta un'entità renderizzabile
/// 
#[derive(Component)]
#[storage(DenseVecStorage)]
pub struct Renderable {
    pub glyph: u8, 
    pub fg: rltk::RGB, 
    pub bg: rltk::RGB
}

///
/// Rappresenta un'entità che può vedere
///
#[derive(Component)]
#[storage(DenseVecStorage)]
pub struct Sight {
    /// Coordinate che sono visibili
    pub visibles: Vec<rltk::Point>,
    /// Quanto distante può vedere
    pub range: i32,
    /// Indica che è stata modificata
    pub dirty: bool
}

///
/// Tagga un'entità che blocca il passaggio sulla
/// propria tile
/// 
#[derive(Component)]
#[storage(DenseVecStorage)]
pub struct Blocker;

///
/// Caratteristiche di combattimento di un'entità
/// 
#[derive(Component)]
#[storage(DenseVecStorage)]
pub struct Stats {
    /// Vita massima
    pub max_hp: i32,
    /// Vita attuale
    pub hp: i32,
    /// Potenza di attacco
    pub atk: i32,
    /// Difesa
    pub def: i32,
}