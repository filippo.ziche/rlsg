use specs::prelude::*;

#[macro_use]
extern crate specs_derive;

mod game;
mod geometry;
mod components;
mod map;
mod spawner;
mod systems;
mod command;
//mod monster;


fn main() {

    // Framework di base
    let ctx = rltk::RltkBuilder::simple80x50()
        .with_title("RLSG")
        .build();

    // Avvia gioco :)
    rltk::main_loop(ctx, game::Game::new());
}
