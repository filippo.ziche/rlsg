use rand::prelude::*;
use rltk::prelude::*;
use specs::prelude::*;
use crate::geometry::{Rect};
use crate::components::*;

///
/// Rappresenta una tile della mappa, può essere
/// solida oppure attraversabile, indica il tipo come parametro aggiuntivo
/// 
#[derive(Copy, Clone, PartialEq)]
pub enum Tile {
    Wall, Floor, Water
}

impl Tile {
    
    /// Indica se questa tile è attraversabile normalmente
    pub fn traversable(&self) -> bool {
        match self {
            Tile::Wall  => false,
            Tile::Floor => true,
            Tile::Water => true,
        }
    }

    /// Indica se questa tile permette di vederci attraverso
    pub fn opaque(&self) -> bool {
        match self {
            Tile::Wall  => true,
            Tile::Floor => false,
            Tile::Water => false,
        }
    }

    /// Ritorna lo stile della tile
    pub fn style(&self) -> (RGB, RGB, u8) {
        match self {
            Tile::Wall  => (RGB::from_f32(0.4, 0.5, 0.6), RGB::named(rltk::BLACK), to_cp437('X')),
            Tile::Floor => (RGB::from_f32(0.2, 0.3, 0.4), RGB::named(rltk::BLACK), to_cp437('.')),
            Tile::Water => (RGB::from_f32(0.4, 0.5, 0.6), RGB::named(rltk::BLACK), to_cp437('~')),
        }
    }
}

/// Larghezza della mappa
pub const MAP_W: usize = 60;
/// Altezza della mappa
pub const MAP_H: usize = 40;

/// In questo contesto un rettangolo rappresenta una stanza
type Room = Rect;

///
/// Rappresenta una mappa e tutte le informazioni relative ad essa,
/// come il numero di entità su una tile, quali tiles sono visibili
/// dal giocatore oppure che sono state già eslorate
/// 
pub struct Map {
    /// Tiles della mappa
    pub tiles: Vec<Tile>,
    /// Stanze che compongono la mappa
    pub rooms: Vec<Room>,
    /// Indica se la tile è occupata da un entità bloccante
    pub occupied: Vec<bool>,
    /// Indica quali entità sono presenti un una tile
    pub entities: Vec<Vec<Entity>>,
    /// Indica tiles esplorate dal giocatore
    pub explored: Vec<bool>,
    /// Indica tiles visibili dal giocatore
    pub sight: Vec<bool>
}

// Costruzione standard e inutile
impl Default for Map {
    fn default() -> Self {
        Self {
            // Inizialmente tutte le tiles sono piene
            tiles: vec![Tile::Wall; MAP_W * MAP_W],
            // Non ci sono stanze all'inizio
            rooms: vec![],
            // Inizialmente nulla è occupato
            occupied: vec![false; MAP_W * MAP_H],
            // Inizialmente non ci sono entità
            entities: vec![vec![]; MAP_W * MAP_H],
            // Inizialmente nessuna tile è stata esplorata
            explored: vec![false; MAP_W * MAP_H], 
            // Inizialmente nessuna tile è visibile
            sight: vec![false; MAP_W * MAP_H]
        }
    }
}

impl Map {
    
    /// Ottiene l'indice all'interno della mappa di una coordinata x, y
    pub fn xy_to_idx(x: i32, y: i32) -> usize {
        (y * (MAP_W as i32) + x) as usize
    }

    /// Indica se una tile è attraversabile
    pub fn traversable(&self, x: i32, y: i32) -> bool {
        let idx = Self::xy_to_idx(x, y);
        self.tiles[idx].traversable() && !self.occupied[idx]
    }

    /// Indica se una tile è visibile (prende in considerazione il giocatore)
    pub fn visible(&self, x: i32, y: i32) -> bool {
        self.sight[Self::xy_to_idx(x, y)]
    }

    /// Indica se una tile è interna alla mappa
    pub fn inside(&self, x: i32, y: i32) -> bool {
        x > 0 && x < (MAP_W as i32 - 1) && y > 0 && y < (MAP_H as i32 - 1)
    }

    /// Aggiorna le tiles bloccate da entità
    pub fn update_occupied_field(&mut self, world: &specs::World) {    
        
        // Resetta campo
        for occupied in self.occupied.iter_mut() { *occupied = false; }

        let blockers  = world.read_storage::<Blocker>();
        let positions = world.read_storage::<Position>();

        // Blocca tiles con un'entità bloccante sopra
        for (position, _) in (&positions, &blockers).join() {
            self.occupied[Self::xy_to_idx(position.x, position.y)] = true;
        }
    }

    /// Aggiorna i campi di visibilità della mappa
    pub fn update_visibility_fields(&mut self, sight: &Sight) {
        // Resetta tiles visibili
        for visible in self.sight.iter_mut() { *visible = false; }

        // Aggiorna tiles esplorate e visibili
        for coord in sight.visibles.iter() {
            self.explored[Self::xy_to_idx(coord.x, coord.y)] = true;
            self.sight[Self::xy_to_idx(coord.x, coord.y)] = true;
        }
    }

    /// Crea una nuova mappa con un numero specificato di stanze collegate
    /// da corridoi
    pub fn create(max_rooms_count: u32) -> Self {
        let mut map = Map::default();

        let mut rng = rand::thread_rng(); 
        for _ in 0..max_rooms_count {

            let rw = rng.gen_range(4_i32, 12);
            let rh = rng.gen_range(4_i32, 12);
            let rx = rng.gen_range(1_i32, (MAP_W as i32) - rw - 1); 
            let ry = rng.gen_range(1_i32, (MAP_H as i32) - rh - 1);

            // Controlla se intersechiamo una stanza già presente
            let new_room = Room::new(rx, ry, rw, rh);
            if !map.rooms.iter().any(|x| x.intersect(&new_room, 1)) {
                map.apply_room(&new_room);

                // Crea corridoio solo se non siamo la prima stanza
                if !map.rooms.is_empty()  {

                    let (beg_x, beg_y) = map.rooms[map.rooms.len() - 1].center();
                    let (end_x, end_y) = new_room.center();
                    
                    map.apply_corridor(beg_x, beg_y, end_x, end_y);
                }

                // Aggiunge stanze
                map.rooms.push(new_room);
            }
        }

        map
    }

    /// Renderizza la mappa
    pub fn render(&self, ctx: &mut rltk::Rltk) {
        for idx in 0..(MAP_W * MAP_H) {
            let (x, y) = ((idx % MAP_W) as i32, (idx / MAP_W) as i32);

            // Renderizza solo tiles esplorate
            if self.explored[idx] {
                let (fg, mut bg, mut glyph) = self.tiles[idx].style();

                // Se è visibile allora usa il suo colore normale, altrimenti solo grigi
                if self.sight[idx] {

                    // Effetto cool sulle pareti
                    if self.tiles[idx] == Tile::Wall {
                        glyph = match rand::random::<bool>() {
                            true => to_cp437('▓'), false => to_cp437('▓')
                        }
                    }
                    
                    ctx.set(x, y, fg, bg, glyph);

                } else {
                    // Greyscale
                    ctx.set(x, y, RGB::from_f32(0.2, 0.2, 0.2), bg, glyph);
                }
            }
        }
    }

    /// Renderizza dati di debug, ad esempio tile bloccanti
    pub fn render_debug_overlay(&self, ctx: &mut Rltk) {
        for idx in 0..(MAP_W * MAP_H) {
            let (x, y) = ((idx % MAP_W) as i32, (idx / MAP_W) as i32);

            // Se occupata
            if self.occupied[idx] {
                ctx.set(x, y, RGB::from_f32(1.0, 1.0, 1.0), RGB::named(rltk::RED), to_cp437('O'));
            }

            // Se non attraversabile
            if !self.tiles[idx].traversable() {
                ctx.set(x, y, RGB::from_f32(1.0, 1.0, 1.0), RGB::named(rltk::RED), to_cp437('B'));
            }
        }
    }

    /// Applica una stanza alla mappa, facendo diventare il suo interno
    /// pavimento
    fn apply_room(&mut self, room: &Room) {
        for x in room.beg_x..room.end_x {
            for y in room.beg_y..room.end_y {
                self.tiles[Self::xy_to_idx(x, y)] = Tile::Floor;
            }
        }
    }

    /// Applica un corridoio alla mappa, facendo diventare il suo percorso
    /// del pavimento
    pub fn apply_corridor(&mut self, beg_x: i32, beg_y: i32, end_x: i32, end_y: i32) {
        
        // In che direzione muoverci
        let dir_x = (end_x - beg_x).signum();
        let dir_y = (end_y - beg_y).signum();
        
        // TODO: Decidere in che ordine eseguire le operazioni, se prima 
        // verticale o se prima orizzontale

        let (mut x, mut y) = (beg_x, beg_y);
        while x != end_x {
            while y != end_y {
                self.tiles[Self::xy_to_idx(x, y)] = Tile::Floor;
                y += dir_y;
            }
            self.tiles[Self::xy_to_idx(x, y)] = Tile::Floor;
            x += dir_x;
        }
    }

    /// ...
    fn is_exit_valid(&self, x:i32, y:i32) -> bool {
        if x < 1 || x > (MAP_W as i32 - 1) || y < 1 || y > (MAP_H as i32 - 1) { return false; }
        return self.traversable(x, y);
    }

    // BUG: i mostri sembrano in grado di entrare nelle celle degli altri mostri diagonalmente...

    /// Produce un vettore di possibili direzioni attraversabili dalla posizione corrente, associando
    /// anche un costo per il pathfinding
    fn exits(&self, x: i32, y: i32) -> Vec<(usize, f32)> {
        let mut exits : Vec<(usize, f32)> = Vec::new();
        
        let idx = Self::xy_to_idx(x, y);
        let w = MAP_W;

        // Cardinali
        if self.is_exit_valid(x-1, y) { exits.push((idx-1, 1.0)) };
        if self.is_exit_valid(x+1, y) { exits.push((idx+1, 1.0)) };
        if self.is_exit_valid(x, y-1) { exits.push((idx-w, 1.0)) };
        if self.is_exit_valid(x, y+1) { exits.push((idx+w, 1.0)) };

        // Diagonali

        if self.is_exit_valid(x-1, y-1) { exits.push(((idx-MAP_W)-1, 1.45)); }
        if self.is_exit_valid(x+1, y-1) { exits.push(((idx-MAP_W)+1, 1.45)); }
        if self.is_exit_valid(x-1, y+1) { exits.push(((idx+MAP_W)-1, 1.45)); }
        if self.is_exit_valid(x+1, y+1) { exits.push(((idx+MAP_W)+1, 1.45)); }

        exits
    }
}

// ==========================================================================================
// ALGORITMI RLTK

impl rltk::BaseMap for Map {
    //NB: non deve basarsi su blocked, ma su cosa è possibile vedere attraverso!
    fn is_opaque(&self, idx:usize) -> bool {
        self.tiles[idx].opaque()
    }
    
    fn get_available_exits(&self, idx: usize) -> Vec<(usize, f32)> {
        let x = idx as i32 % MAP_W as i32;
        let y = idx as i32 / MAP_W as i32;
        self.exits(x, y)
    }
}

impl rltk::Algorithm2D for Map {
    fn dimensions(&self) -> rltk::Point {
        Point::new(MAP_W, MAP_H)
    }
}

// ==========================================================================================
// UTILITIES
