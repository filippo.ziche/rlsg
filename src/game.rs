use crate::components::*;
use crate::command::*;
use crate::systems::*;
use crate::spawner;
use crate::map::*;
use specs::prelude::*;
use rand::prelude::*;
use rltk::prelude::*;
use rltk::{RandomNumberGenerator};
use std::cmp::{max, min};

///
/// Messaggi del gioco
/// 
#[derive(Default)]
pub struct GameLog {
    pub messages: Vec<String>
}

///
/// Indica in che stato si trova il gioco
/// ad esempio in un menu o aspettando input
/// 
#[derive(Copy, Clone, PartialEq)]
pub enum RunState { Paused, Running }

///
/// Rappresenta lo stato del gioco e le variabili globali principali
/// 
pub struct Game {
    /// ECS system by specs
    pub world: specs::World,
    /// Identificatore del giocatore, per modificarlo direttamente
    pub player: Option<specs::Entity>,
    // Stato corrente del gioco
    pub runstate: RunState
}

/// Permette al gioco di essere usato da RLTK
impl rltk::GameState for Game {
    fn tick(&mut self, ctx: &mut rltk::Rltk) {
        
        // Cambia comportamento in base al nostro runstate
        match self.runstate {
            // Singolo aggiornamento
            RunState::Running => {
                self.update(ctx);
                self.runstate = RunState::Paused;
            }

            // Attende input e avanza solo se lo riceve
            RunState::Paused => {
                self.runstate = self.input(ctx);
            }
        }

        self.render(ctx);
    }
}

impl Game {

    /// Crea un nuovo gioco invocando 'initialize'
    pub fn new() -> Self {
        let mut res = Self {
            world: specs::World::new(),
            player: None,
            runstate: RunState::Running
        };

        //Inizializza gioco
        res.initialize();
        res
    }

    /// Inizializza il gioco
    fn initialize(&mut self) {

        // Registra tutti i componenti usati
        self.world.register::<Position>();
        self.world.register::<Renderable>();
        self.world.register::<Sight>();
        self.world.register::<Player>();
        self.world.register::<Monster>();
        self.world.register::<Name>();
        self.world.register::<Blocker>();
        self.world.register::<MoveCommand>();
        self.world.register::<MeleeAttackCommand>();
        self.world.register::<Damage>();
        self.world.register::<Stats>();
    
        // Genera mappa casuale
        let mut map = Map::create(50);
        
        // Crea il giocatore
        let (start_x, start_y) = map.rooms[0].center();
        self.player = Some(spawner::player(start_x, start_y, &mut self.world));

        // Aggiunge id del giocatore alle variabili globali
        // TODO: trova metodo migliore
        self.world.insert::<specs::Entity>(self.player.unwrap());

        // Crea un po di nemici dentro le stanze
        for room in map.rooms.iter().skip(1) {
            let (center_x, center_y) = room.center();
            spawner::enemy(center_x, center_y, &mut self.world);
        }

        // Salva mappa nelle risorse
        map.update_occupied_field(&self.world);
        self.world.insert::<Map>(map);

        // Aggiunge gamelog
        self.world.insert::<GameLog>(GameLog::default());
    }

    /// Aggiorna il gioco
    fn update(&mut self, ctx: &mut rltk::Rltk) {

        // Resetta messaggi
        {
            let mut log = self.world.fetch_mut::<GameLog>();
            log.messages.clear();
        }
        
        /*
        //  ... Beh ... belle queste dipendenze
        let mut dispatcher = DispatcherBuilder::new()
            .with(SightSystem, "sight", &[])
            .with(MonsterSystem, "monsters", &["sight"])
            .with(MoveSystem, "move", &["monsters"])
            .with(MeleeAttackSystem, "melee", &["monsters"])
            .with(DamageSystem, "damage", &["melee"])
            .build();

        // Registra componenti dei sistemi
        dispatcher.setup(&mut self.world);

        // Esegue tutti i sistemi in parallelo
        dispatcher.dispatch(&self.world);
        */

        // Aggiorna mappa di visibilità delle entità nella scena
        let mut sight_system = SightSystem;
        sight_system.run_now(&self.world);

        // Aggiorna posizione mostri e causa attacchi
        let mut monster_system = MonsterSystem;
        monster_system.run_now(&self.world);

        // Esegue azioni di movimento create dall'AI
        let mut move_system = MoveSystem;
        move_system.run_now(&self.world);

        // Esegue combattimento melee
        let mut melee_system = MeleeAttackSystem;
        melee_system.run_now(&self.world);

        // Applica danno e rimuove entità morte
        let mut damage_system = DamageSystem;
        damage_system.run_now(&self.world);
        DamageSystem::remove_dead(&mut self.world);

        // Aggiorna tiles bloccate della mappa
        // NB: i mostri aggiornano dinamicamente questo campo, non serve aggiornarlo ogni
        // update ma solo all'inizioe
        {
            let mut map = self.world.write_resource::<Map>();
            map.update_occupied_field(&self.world);
        }

        self.world.maintain();
    }

    /// Renderizza tutti gli oggetti della scena
    fn render(&mut self, ctx: &mut rltk::Rltk) {

        // Pulisce lo schermo
        ctx.cls();

        // Renderizza la mappa
        let map = self.world.fetch::<Map>();
        map.render(ctx);

        // Posizioni e renderables
        let rs = self.world.read_storage::<Renderable>();
        let ps = self.world.read_storage::<Position>();

        // Renderizza tutti gli oggetti della scena visibili
        for (pos, sprite) in (&ps, &rs).join() {
            if map.visible(pos.x, pos.y) {
                ctx.set(pos.x, pos.y, sprite.fg, sprite.bg, sprite.glyph);
            }
        }

        self.render_ui(ctx);

        // Overlay di debug per la mappa
        //map.render_debug_overlay(ctx);
    }

    fn render_ui(&self, ctx: &mut rltk::Rltk) {
        let player = self.world.fetch::<Entity>();

        let stat = self.world.read_storage::<Stats>();
        let stat = stat.get(*player).unwrap();

        // Message box
        ctx.draw_box(0, 40, 60, 9, RGB::named(rltk::WHITE), RGB::named(rltk::BLACK));
        ctx.print_color(24, 40, RGB::named(rltk::YELLOW), RGB::named(rltk::BLACK), " rlgs by z1ko ");
        
        // Renderizza messaggi se presenti
        let log = self.world.fetch::<GameLog>();
        let mut y = 0_i32;
        for msg in log.messages.iter() {
            ctx.print_color(1, 41 + y, RGB::named(rltk::WHITE), RGB::named(rltk::BLACK), msg);
            y = (y + 1) % 5;
        }


        // Generic
        ctx.draw_box(61, 0, 18, 39, RGB::named(rltk::WHITE), RGB::named(rltk::BLACK));


        // Stats
        ctx.draw_box(61, 40, 18, 9, RGB::named(rltk::WHITE), RGB::named(rltk::RED));
        ctx.print(62, 42, &format!("  HP  {} / {}", stat.hp, stat.max_hp));
        ctx.print(62, 44, &format!(" ATK  {}", stat.atk));
        ctx.print(62, 46, &format!(" DEF  {}", stat.def));
    }

    /// Prova a spostare il giocatore nella mappa
    fn try_move_player(&mut self, dx: i32, dy: i32) {

        // Ottiene posizione del giocatore
        let mut positions = self.world.write_storage::<Position>();
        if let Some(position) = positions.get_mut(self.player.expect("Deve esserci il giocatore")) {
            let (dest_x, dest_y) = (position.x + dx, position.y + dy);
            let dest_idx = Map::xy_to_idx(dest_x, dest_y);

            // Controlla se la posizione è attraversabile e in caso positivo la setta
            let map = self.world.fetch::<Map>();
            if map.traversable(dest_x, dest_y) {

                // Aggiorna posizione
                position.x = max(0, min(dest_x, MAP_W as i32));
                position.y = max(0, min(dest_y, MAP_H as i32));

                // Aggiorna la visione se possibile
                let mut sight = self.world.write_storage::<Sight>();
                if let Some(sight) = sight.get_mut(self.player.unwrap()) {
                    sight.dirty = true;
                }
            }
            // Possibile attacco
            else if !map.entities[dest_idx].is_empty() {
                let mut attacks = self.world.write_storage::<MeleeAttackCommand>();
                attacks.insert(self.player.unwrap(), MeleeAttackCommand { 
                    target: map.entities[dest_idx][0]
                }).expect("Impossibile aggiungere comando di attacco contro il mostro");
            }
        }
    }

    /// Reagisce all'input
    fn input(&mut self, ctx: &mut rltk::Rltk) -> RunState {
        if let Some(key) = ctx.key {
            match key {
                // Movimento
                VirtualKeyCode::Left  => self.try_move_player(-1, 0),
                VirtualKeyCode::Right => self.try_move_player( 1, 0),
                VirtualKeyCode::Up    => self.try_move_player( 0,-1),
                VirtualKeyCode::Down  => self.try_move_player( 0, 1),

                // Non abbiamo ricevuto input rilevante
                _ => { return RunState::Paused; }
            }
            // Abbiamo eseguito un'azione quindi avanziamo stato
            return RunState::Running;
        }
        // Non è stato premuto nessun tasto
        return RunState::Paused;
    }
}