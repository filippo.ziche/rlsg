
type World;

///
/// Tipi di mostro nel gioco:
///     goblin
///     troll
///     bat
///     rat
///     succubus
///     slime
///     vampire
///     animorph
///     undead
/// 

/// Comportamento di un entità
trait Brain {
    /// Esegue il turno, ottiene l'entità a cui è collegato e il mondo
    fn update(entity: &Entity, world: &World) -> Action;
}

/// Cervello base che cerca di avvicinarsi al giocatore e poi lo attacca
pub struct StandardBrain;
impl Brain for StandardBrain {
    fn update(&self, entity: &Entity, world: &World) -> Action {
        // Se vediamo il giocatore e siamo distanti 
        if entity.visible(world.player.position) && geom::distance(world.player.position, entity.position) > 1.5 {
            // Avvicinati al giocatore
            return Action::MoveTo(world.player.position);
        }
        // Attacca il giocatore
        return Action::Attack(world.player);
    }
}

struct Entity {

    /// Posizione nella mappa
    pub x: i32, 
    pub y: i32,

    /// Informazioni grafiche
    pub glyph: char,
    pub fg: RGB,
    pub bg: RGB,

    /// Tiles visibili
    pub visibles: Vec<(i32, i32)>,

    /// Comportamento di questo mostro
    pub brain: Option<dyn Brain>

    /// Visione di questo mostro
    pub sight: Option<dyn Sight>
}

impl Entity {
    /// controlla se un entità è visibile dalla posizione attuale
    fn visible(&self, position: (i32, i32)) -> bool {
        self.visibles.iter().any(|(x,y)| x == position.0 && y == position.1)
    }
}

fn spawn_goblin() -> Entity {
    Entity {
        x: 0, y: 0,
        glyph: 'g', fb: RGB::GREEN, bg: RGB::BLACK,
        brain: Some(StandardBrain)
    }
}