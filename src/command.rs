use crate::components::*;
use crate::map::*;
use crate::game::GameLog;
use specs::prelude::*;

///
/// I comandi sono dei componenti, quindi possono essere attaccati ad
/// un entità e lasciare che siano i vari sistemi a gestirlo
/// Ad esempio per far muovere un entitò basta che questa abbia una posizione e un
/// MoveCommand e il sistema di movimento eseguirà l'azione...
/// 

///
/// Ordina un movimento
///
#[derive(Component)]
pub struct MoveCommand {
    /// Idx della nuova posizione desiderata
    pub new_idx: usize
}

/// Gestisce il movimento delle entità nella mappa, 
/// eseguie A* pathfinding per arrivare alla nuova posizione desiderata
pub struct MoveSystem;
impl <'a> System<'a> for MoveSystem {

    ///
    /// Map: permette di eseguire query sulla mappa e sulle tiles attraversabili
    /// Entities: permette di salvare le entità nelle celle della mappa
    /// Position: permette di modificare la posizione
    /// 
    type SystemData = (WriteExpect<'a, Map>, Entities<'a>, WriteStorage<'a, Position>, WriteStorage<'a, MoveCommand>);
    
    // Esegue il comando
    fn run(&mut self, (mut map, entities, mut positions, mut commands): Self::SystemData) {
        for (entity, position, command) in (&entities, &mut positions, &commands).join() {
            let old_idx = Map::xy_to_idx(position.x, position.y);

            // Cerca percorso per il target ed esegue il primo step
            let path = rltk::a_star_search(old_idx, command.new_idx, &mut *map);
            if path.success && path.steps.len() > 1 {

                let new_x = (path.steps[1] % MAP_W ) as i32;
                let new_y = (path.steps[1] / MAP_W ) as i32;

                // Aggiorna mappa di occupazione per evitare che altri mostri durante l'update di questo sitema
                // vedano la nuova posizione di questo mostro come libera...
                map.occupied[Map::xy_to_idx(position.x, position.y)] = false;
                map.occupied[Map::xy_to_idx(new_x, new_y)] = true;

                // Aggiorna vettore entità per la cella
                map.entities[Map::xy_to_idx(position.x, position.y)].retain(|e| *e != entity);
                map.entities[Map::xy_to_idx(new_x, new_y)].push(entity);

                // Aggiona posizione mostro
                position.x = new_x;
                position.y = new_y;
            }
        }

        // Rimuove i comandi perché sono stati eseguiti
        commands.clear();
    }
}

///
/// Danno ad un'entità
/// 
#[derive(Component)]
pub struct Damage {
    pub amount: i32
}

///
/// Applica danno alle entità
/// NB: le entità morte non vengono rimosse, quello è uno step successivo nel gioco
/// 
pub struct DamageSystem;
impl <'a> System<'a> for DamageSystem {

    ///
    /// Stats:      permette di assegnare il danno
    /// Damage:     danno da applicare alle entità
    /// Names:      per generare messaggi comprensibili
    /// 
    type SystemData = (Write<'a, GameLog>, WriteStorage<'a, Stats>, WriteStorage<'a, Damage>, ReadStorage<'a, Name>);

    fn run(&mut self, (mut log, mut stats, mut damages, names): Self::SystemData) {
        for (stat, damage, name) in (&mut stats, &damages, &names).join() {
            stat.hp -= damage.amount;

            log.messages.push(format!("{} subisce {} danni! ({}/{})", name.0, damage.amount, stat.hp, stat.max_hp));
            if stat.hp <= 0 {
                log.messages.push(format!("{} è stato ucciso, una creatura in meno per le tenebre!", name.0));
            }
        }

        // Rimuove comandi eseguiti
        damages.clear();
    }

}

impl DamageSystem {
    pub fn remove_dead(ecs : &mut World) {
        let mut dead : Vec<Entity> = Vec::new();
        {
            let combat_stats = ecs.read_storage::<Stats>();
            let entities = ecs.entities();
            for (entity, stats) in (&entities, &combat_stats).join() {
                if stats.hp < 1 { dead.push(entity); }
            }
        }
    
        for victim in dead {
            ecs.delete_entity(victim)
                .expect("Unable to delete");
        }
    }
}



///
/// Attacca un entità
///
#[derive(Component)]
pub struct MeleeAttackCommand {
    pub target: Entity
}

/// Gestisce il combattimento melee delle entità nella mappa
/// calcolando i danni effettivi etc
pub struct MeleeAttackSystem;
impl <'a> System<'a> for MeleeAttackSystem {

    ///
    /// Stats:      permette di calcolare le caratteristiche di attacco
    /// Damage:     assegna un danno da applicare alle entità
    /// Names:      per generare messaggi comprensibili
    /// 
    type SystemData = (Write<'a, GameLog>, ReadStorage<'a, Stats>, WriteStorage<'a, Damage>,
        ReadStorage<'a, Name>, WriteStorage<'a, MeleeAttackCommand>);

    /// Causa danni
    fn run(&mut self, (mut log, stats, mut damages, names, mut attacks): Self::SystemData) {
        for (self_stat, name, attack) in (&stats, &names, &attacks).join() {
            
            // Se l'esecutore è ancora vivo
            if self_stat.hp > 0 {

                // Ottiene statistiche del target se possibile, altrimenti amen,
                // l'attacco non aveva senso
                // inoltre controlla che non sia già morto
                if let Some(target_stat) = stats.get(attack.target) {
                    if target_stat.hp > 0 {

                        // Se il targe ha un nome allora mostra messaggio
                        if let Some(target_name) = names.get(attack.target) {
                            log.messages.push(format!("{} (atk: {}) attacca {} (def: {})", name.0, self_stat.atk, target_name.0, target_stat.def));
                        }

                        // Genera danno
                        let hit = i32::max(0, self_stat.atk - target_stat.def);
                        damages.insert(attack.target, Damage { amount: hit })
                            .expect("Impossibile infliggere danno");
                    }
                }
            }
            
        }

        // Rimuove i comandi perché sono stati eseguiti
        attacks.clear();
    }
}

